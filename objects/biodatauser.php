<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "biodata";
 
    // object properties
    public $id_biodata;
    public $nama;
    public $klass;
    public $tanggal_lahir;
    public $alamat;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    // signup user
    function lengkapi_biodata(){
    
        if($this->isAlreadyExist()){
            return false;
        }
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                nama=:nama, klass=:klass, tanggal_lahir=:tanggal_lahir, alamat=:alamat";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->nama=htmlspecialchars(strip_tags($this->nama));
        $this->klass=htmlspecialchars(strip_tags($this->klass));
        $this->tanggal_lahir=htmlspecialchars(strip_tags($this->tanggal_lahir));
        $this->alamat=htmlspecialchars(strip_tags($this->alamat));
    
        // bind values
        $stmt->bindParam(":nama", $this->nama);
        $stmt->bindParam(":klass", $this->klass);
        $stmt->bindParam(":tanggal_lahir", $this->tanggal_lahir);
        $stmt->bindParam(":alamat", $this->alamat);
    
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return true;
        }
    
        return false;
        
    }
   
    function isAlreadyExist(){
        $query = "SELECT *
            FROM
                " . $this->table_name . " 
            WHERE
                id_biodata='".$this->id_biodata."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if($stmt->rowCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }
}