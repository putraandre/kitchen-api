<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  // get database connection
  include_once '../config/database.php';
  
  // instantiate user object
  include_once '../objects/biodatauser.php';
  
  $database = new Database();
  $db = $database->getConnection();
  
  $user = new User($db);
  
  // set user property values
  $user->nama = $_POST['nama'];
  $user->klass = $_POST['klass'];
  $user->tanggal_lahir =$_POST['tanggal_lahir'];
  $user->alamat =$_POST['alamat'];
  
  // create the user
  if($user->lengkapi_biodata()){
      $user_arr=array(
          "status" => true,
          "message" => "biodata sukses di lengkapi",
          "nama" => $user->nama
      );
  }
  else{
      $user_arr=array(
          "status" => false,
          "message" => "biodata gagal di lengkapi"
      );
  }
  print_r(json_encode($user_arr));
?>