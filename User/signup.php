<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  // get database connection
  include_once '../config/database.php';
  
  // instantiate user object
  include_once '../objects/user.php';
  
  $database = new Database();
  $db = $database->getConnection();
  
  $user = new User($db);
  
  // set user property values
  $user->username = $_POST['username'];
  $user->email = $_POST['email'];
  $user->password = base64_encode($_POST['password']);
  
  // create the user
  if($user->signup()){
      $user_arr=array(
          "status" => true,
          "message" => "Successfully Signup!",
          "id" => $user->id,
          "username" => $user->username
      );
  }
  else{
      $user_arr=array(
          "status" => false,
          "message" => "Username already exists!"
      );
  }
  print_r(json_encode($user_arr));
?>